from os import getenv


def connection():
    DB = getenv('FLASK_DB')
    if DB == "mysql":
        from mysql.connector import connect as dbconnect
        port = 3306
    elif DB == "postgres":
        from psycopg2 import connect as dbconnect
        port = 5432
    else:
        raise Exception("Database not Supported (only mysql and postgres supported)")
    # format:
    # "localhost;port;username;pasword;database"
    DB_STRING = getenv('FLASK_DB_STRING')
    db_string_lst = DB_STRING.split(";")
    # missing port (use default)
    if db_string_lst[1] == "":
        db_string_lst[1] = port

    connection = dbconnect(
        host = db_string_lst[0],
        port = db_string_lst[1],
        user = db_string_lst[2],
        password = db_string_lst[3],
        database = db_string_lst[4]
    )
    cursor = connection.cursor()
    return cursor, connection
