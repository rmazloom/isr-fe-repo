# Adding index to any JSON file
First remove all the newlines and have each record be on a new line
Next above each line add a header with a counting ID

```{ "index" : { "_index" : "test", "_type" : "type1", "_id" : "1" } }```

You can use the script below to automate it

```
c=0;while read p; do \
echo "{ \"index\" : { \"_id\" : \"$c\" } }" >> indexed.json; \
echo "$p" >> indexed.json; \
c=$(($c + 1)); echo $c;done<cleaned.json
```

# Adding Test Dataset to Elasticsearch
```shell script
curl -s \
-H "Content-Type: application/x-ndjson" \
-XPOST http://localhost:9200/30k/_bulk \
--data-binary @indexed.json; echo
```

# To make sure it went through
```shell script
curl -s -H "Content-Type: application/x-ndjson" \
-XGET http://localhost:9200/30k/_count?pretty -d \
'{
  "query": {
    "match_all": {}
  }
}'
```

# Delete all documents
```shell script
curl -s -H "Content-Type: application/x-ndjson" \
-XPOST http://localhost:9200/30k/_delete_by_query?pretty -d \
'{
  "query": {
    "match_all": {}
  }
}'
```