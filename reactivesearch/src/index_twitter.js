import React, {Component} from "react";
import ReactDOM from "react-dom";
import moment from 'moment';
import {
    CategorySearch,
    TagCloud,
    ReactiveBase,
    ResultList,
    ReactiveList,
    MultiList,
    MultiDropdownList,
    DataSearch,
    SelectedFilters,
    DateRange
} from "@appbaseio/reactivesearch";
import axios from "axios";
import "./styles.css";
import config from 'react-global-configuration';
import $ from 'jquery';
import App from "./App";

const {ResultListWrapper} = ReactiveList;

var advanced_query = ["Brands", "Witness_Name", "Person_Mentioned", "Organization_Mentioned", "Title", "Topic"];


export default class TwitterApp extends Component {
    render() {
        return (
            <ReactiveBase
                app="fe/twt"
                url={config.get('elasticsearch')}
                >
                <div style={{display: "flex", flexDirection: "row"}}>
                    <div style={{display: "flex", flexDirection: "column", width: "40%"}}>
                        <DataSearch
                            componentId="searchbox"
                            autoSuggest={false}
                            dataField={["text","extended_tweet.full_text"]}
                            placeholder="Search for twitter"
                            highlight={true}
                            style={{
                                padding: "5px",
                                marginTop: "10px"
                            }}
                        />
                        {/*search*/}
                        <DateRange
                            componentId="DateSensor"
                            dataField="created_at"
                        />
                        {/*choose date*/}
                        <TagCloud
                            componentId="Hashtagsfilter"
                            dataField="entities.hashtags.text.keywords"
                            title="Hash tags"
                            size={32}
                            showCount={true}
                            multiSelect={true}
                            queryFormat="or"
                            react={{
                                and: ["searchbox", "DateSensor"]
                            }}
                            showFilter={true}
                            filterLabel="Hashtags"
                            URLParams={false}
                            loader="Loading ..."
                        />
                        {/*hash tag cloud*/}
                    </div>
                    <div className="col">
                        <SelectedFilters/>
                        <ReactiveList
                            componentId="SearchResult"
                            dataField="created_at"
                            className="result-list-container"
                            from={0}
                            size={5}
                            pagination={true}
                            renderItem={this.twitterReactiveList}
                            react={{
                                and: ["searchbox", "DateSensor","Hashtagsfilter"],
                            }}
                        />
                    </div>
                </div>
            </ReactiveBase>
        );
    }

    twitterReactiveList(data) {
        return (
            <div className="flex book-content card card-body" key={data._id}>

                <div className="flex column justify-center" style={{marginLeft: 20}}>
                    <div className="book-header"
                         dangerouslySetInnerHTML={{
                             __html: "<b>" + data.user.name + ": </b>" + data.text
                         }}
                         />
                        <span className="pub-year"dangerouslySetInnerHTML={{
                            __html: "<b>Posted:</b>" + data.created_at
                        }}
                        />
                </div>
            </div>
        );
    }


}

