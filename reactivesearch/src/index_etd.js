/*
 * @Author: Chris
 * Created Date: 2019-10-22 14:33:57
 * -----
 * Last Modified: 2020-10-05 13:01:03
 * Modified By: rmazloom
 * -----
 * Copyright (c) 2019
 */

import React, { Component } from "react";
import ReactDOM from "react-dom";
import moment from 'moment';
import {
    ReactiveBase,
    ResultList,
    ReactiveList,
    MultiList,
    MultiDropdownList,
    DataSearch,
    SelectedFilters,
    DateRange,
} from "@appbaseio/reactivesearch";
import axios from "axios";
import "./styles.css";
import config from 'react-global-configuration';

const { ResultListWrapper } = ReactiveList;


var advanced_query = ["degree-level", "contributor-department", "contributor-author",
    "contributor-committeechair", "contributor-committeecochair",
    "contributor-committeemember", "date-available", "date-issued",
    "degree-name", "description-abstract", "Author Email", "subject-none",
    "title-none", "type-none"];

// Search page for ETD
class Etd extends Component {
    render() {
        return (
            <ReactiveBase
                app="fe/etd"
                // credentials="egdxpZGTu:54c431d1-6a44-44b8-b84a-e46c4fed2de6"
                url={config.get('elasticsearch')}
                theme={{
                    typography: {
                        fontFamily:
                            '"Lato", "Open Sans", "Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Noto Sans", "Ubuntu", "Droid Sans", "Helvetica Neue", sans-serif'
                    }
                }}
                /*transformRequest={request => {
                    // Auto-suggestions start from 3rd characters
                    console.log("object 1: %O", request);
                    var request_body = request.body.split('\n');
                    var body_preference = JSON.parse(request_body[0]);
                    var body_query = JSON.parse(request_body[1]);
                    if (body_preference.preference === "List" || body_preference === "search") {
                        var searchText = document.getElementById("search-downshift-input").value;
                        var sT = searchText.split(":");

                        if (sT.length > 1) //the first part of the split should be the relevant field(s)
                        {
                            var fields = sT[0].split("+");
                            var newfieldsinput = "[";
                            for (var i = 0; i < fields.length; i++) {
                                newfieldsinput = newfieldsinput + "\"" + fields[i] + "\"";
                                if (i !== fields.length - 1) { newfieldsinput += ","; }

                            }
                            newfieldsinput += "]";
                            // request.body = request.body.replace("[\"Brands\",\"Witness_Name\",\"Person_Mentioned\",\"Organization_Mentioned\",\"Title\",\"Topic\"]", newfieldsinput );
                            //Future work: make a function to put the fields in a variable instead of hardcoding
                            request.body = request.body.replace("[\"degree-level\",\"contributor-department\",\"contributor-author\",\"contributor-committeechair\",\"contributor-committeecochair\",\"contributor-committeemember\",\"date-available\",\"date-issued\",\"degree-name\",\"description-abstract\",\"Author Email\",\"subject-none\",\"title-none\",\"type-none\"]", newfieldsinput);

                        }


                        console.log("object 2: %O", request);

                        if (body_preference.preference === "search") {
                            if (body_query.query.bool.must[0].bool.must[0].bool.should[0].multi_match.query.length < 3) {
                                return null;
                            }
                        }

                        // Post logs
                        client({
                            method: 'post',
                            url: '/emitlogs',
                            data: JSON.stringify(request),
                            headers: {
                                "Content-Type": "application/json",
                            }
                        });
                        return request
                    }
                }}*/ //TODO: replace with apiClient function to send request to back-end (Flask API)
            >
                <div className="container-fluid fek-searching">
                    <div className="searchbar">
                        <DataSearch
                            componentId="search"
                            dataField={["contributor-department", "contributor-author",
                                "contributor-committeechair", "contributor-committeecochair",
                                "contributor-committeemember", "degree-name", "description-abstract",
                                "Author Email", "subject-none", "title-none"]}
                            /*customQuery={
                                function (value, props) {
                                    return {
                                        query: {
                                            multi_match: {
                                                query: value,
                                                fields: advanced_query2
                                            }
                                        }
                                    }
                                }
                            }*/
                            fieldWeights={[1, 3, 3, 3, 1, 1, 1, 1, 1, 5]}
                            react={{
                                and: ["filter_degree", "filter_type", "filter_date-issued"]
                            }}
                            autoFocus={true}
                            fuzziness={0}
                            // debounce={100}
                            highlight={true}
                            placeholder="Search ETD"
                            title="Search for ETD"
                            showClear={true}
                            autosuggest={true}
                            showVoiceSearch={true}
                            queryFormat={"and"}


                        />
                    </div>

                    <div className="container-fluid">
                        <div className={"row"}>
                            <div className={"sidebar"}>

                                <MultiList
                                    componentId="filter_type"
                                    dataField={"type-none.keyword"}
                                    title="Type"
                                    size={100}
                                    queryFormat="or"
                                    showCheckbox={true}
                                    showCount={true}
                                    showFilter={true}
                                    filterLabel="Type"
                                    loader="Types Loading ..."
                                    showSearch={false}
                                    react={{
                                        and: ["filter_degree", "search", "filter_date-issued"]
                                    }}
                                />

                                <MultiList
                                    componentId="filter_degree"
                                    dataField={"degree-level.keyword"}
                                    title="Degree"
                                    size={100}
                                    queryFormat="or"
                                    showCheckbox={true}
                                    showCount={true}
                                    showFilter={true}
                                    filterLabel="Degree"
                                    loader="Degrees Loading ..."
                                    showSearch={false}
                                    react={{
                                        and: ["search", "filter_type", "filter_date-issued"]
                                    }}
                                />

                                <DateRange
                                    className={"daterange"}
                                    innerClass={{
                                        "input-container": 'date-container'
                                    }}
                                    componentId="filter_date-issued"
                                    dataField="date-issued"
                                    title="Date Issued"
                                    focused={false}
                                    autoFocusEnd={true}
                                    numberOfMonths={1}
                                    initialMonth={new Date('2017-01-01')}
                                />

                            </div>

                            <div className={"col"}>
                                <SelectedFilters
                                    className={"row"}
                                    showClearAll={true}
                                    clearAllLabel="Clear filters"
                                />
                                <ReactiveList
                                    componentId="List"
                                    dataField="Title"

                                    className="result container-fluid"
                                    size={10}
                                    loader="Loading Results ..."
                                    react={{
                                        and: ["filter_type", "filter_degree", "search", "filter_date-issued"]
                                    }}
                                    infiniteScroll={true}
                                    //pagination={true}
                                    //paginationAt={"both"}
                                    renderItem={this.showResults}

                                />
                            </div>
                        </div>
                    </div>
                </div>
            </ReactiveBase >
        );
    }


    // Used for each item named: res
    showResults(res){
        return (
            <ResultListWrapper>
                    <ResultList key={res._id}>
                        {/* <ResultList.Image src={res.image} /> */}
                        <ResultList.Content className={"container-fluid"}>
                            {/* ETD title and embedded link */}
                            <ResultList.Title style={{whiteSpace: "normal"}}>
                                <div className="book-title">
                                    <a href={res["identifier-uri"]} target="_blank"
                                       dangerouslySetInnerHTML={{
                                           __html: res["title-none"]
                                       }}
                                    />
                                </div>
                            </ResultList.Title>
                            <ResultList.Description className={"container-fluid"}>
                                <div>
                                    <div>
                                        <div>
                                            {/*  Authors + committee */}
                                            <div
                                                className="authors-list row"
                                                dangerouslySetInnerHTML={{
                                                    __html: res["contributor-author"] + ', ' + res["contributor-committeechair"] + ', ' + res["contributor-committeecochair"] + ', ' + res["contributor-committeemember"],
                                                }}
                                            />
                                        </div>
                                    </div>
                                    {/* Type and date + abstract showing button */}
                                    <div className={"row"}>
                                        <div className={"col align-self-start"}
                                             style={{whiteSpace: "pre-wrap"}}
                                             dangerouslySetInnerHTML={{
                                                 __html: res["type-none"] + " <b>Date Issued:</b> " + res["date-issued"],
                                             }}
                                        />
                                        <a className="col-3 align-self-end text-right"
                                           data-toggle="collapse"
                                           href={"#card" + res["handle"]}
                                           role="button" aria-expanded="false"
                                           aria-controls={"card" + res["handle"]}>
                                            View Abstract
                                        </a>
                                    </div>
                                    {/* Keywords concatinated list */}
                                    <div className="align-self-start col"
                                         style={{paddingLeft: 0}}
                                         dangerouslySetInnerHTML={{
                                             __html: "<b>Keywords:</b> " + res["subject-none"]
                                         }}
                                    />
                                    {/* collapsable abstract (initially hidden) */}
                                    <div className="collapse"
                                         id={"card" + res["handle"]}>
                                        <div className="card-body"
                                             dangerouslySetInnerHTML={{
                                                 __html: res["description-abstract"]
                                             }}
                                        />
                                    </div>
                                </div>
                            </ResultList.Description>
                        </ResultList.Content>
                    </ResultList>
            </ResultListWrapper>
        )
    }
}

export default Etd;