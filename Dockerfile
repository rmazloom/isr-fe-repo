FROM node:latest

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc && \
    find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
    /opt/conda/bin/conda clean -afy

COPY . /app

WORKDIR /app/reactivesearch


RUN /opt/conda/bin/conda init bash && \
	. /root/.bashrc && \
	conda create -n env python=3.6 && \
	conda activate env && \
	pip install -r /app/requirements.txt && \
	npm install

WORKDIR /app

EXPOSE 3000

CMD . /root/.bashrc && \
	cd /app/reactivesearch && \
	npm run build && \
	cd /app && \
	conda activate env && \
	env && \
	python app.py

# CMD /bin/bash



	

	
	
	
