
CREATE DATABASE fe;
USE fe;
CREATE TABLE `users` (
  `user_type` varchar(20) NOT NULL DEFAULT 'user',
  `username` varchar(100) NOT NULL,
  `email` varchar(500) NOT NULL,
  `register_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(500) NOT NULL
);